import logging
import logging.handlers as handlers
import time


def getLoggers(fileName):
	logger = logging.getLogger(fileName)
	logger.setLevel(logging.INFO)

	# Here we define our formatter
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

	# logHandler = handlers.TimedRotatingFileHandler('../logs/UdeDowner-info.log', when='M', interval=1, backupCount=0)
	logHandler = handlers.RotatingFileHandler('../logs/UdeDowner-info.log', maxBytes=20000, backupCount=0)

	logHandler.setLevel(logging.INFO)
	logHandler.setFormatter(formatter)

	errorLogHandler = handlers.RotatingFileHandler('../logs/UdeDowner-error.log', maxBytes=5000, backupCount=0)
	errorLogHandler.setLevel(logging.ERROR)
	errorLogHandler.setFormatter(formatter)

	logger.addHandler(logHandler)
	logger.addHandler(errorLogHandler)

	return logger;