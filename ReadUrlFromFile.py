import sys  
import os
import re
from Logger import getLoggers

LOGGER = getLoggers("ReadUrlFromFile");

URL_REGEX = "https:\/\/\w+-\w+-\w+-\w+.udemy.com\/(.+)\.mp4\?nva=\d+&token=[a-zA-Z0-9]+\"}"

def extractUrlList(pathFolder,nameFile):
    
    LOGGER.info("[==>] extractUrlList")

    filepath = pathFolder+"/"+nameFile
    uniqueUrlList = []
    
    if not os.path.isfile(filepath):
        LOGGER.error("File path {} does not exist. Exiting ...".format(filepath))
        return;

    with open(filepath) as fp:
        cnt = 0
        for line in fp:
            x = re.search(URL_REGEX, line)
            if not (x is None): 
                urlStr = x.group()[:-2]
                if urlStr not in uniqueUrlList: 
                    uniqueUrlList.append(urlStr)
    return uniqueUrlList
