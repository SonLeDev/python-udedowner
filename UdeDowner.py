import glob, os 
import logging

from ReadUrlFromFile import extractUrlList
from RequestDownloadUrl import requestDownload
from Logger import getLoggers

DIR_META_URL            = "/Volumes/DEV/Download/01aMetaUrl"
EXT_CURL                = "*.curl"
EXT_FILE_PROCESSING     = "PROCESSING"
EXT_FILE_DONE           = "DONE"
SAVE_AS                 = "/Volumes/DEV/Download"

LOGGER = getLoggers("UdeDowner");

def touchFile(file,ext):
    os.rename(file,file+"."+ext)
    return file+"."+ext
    
def main():

    print("*********** [START] UdeDowner App **************")
    LOGGER.info("===========================================================")
    LOGGER.info("=======  Welcome UdeDowner App  ======")
    LOGGER.info("===========================================================")
    
    os.chdir(DIR_META_URL)
    LOGGER.info("[READ] meta from {}".format(DIR_META_URL))
    count = 0;
    for file in glob.glob(EXT_CURL):
    
        LOGGER.info("[FOUND] a file {}".format(file))
        print("[FOUND] a file {}".format(file))

        processingFile = touchFile(file,EXT_FILE_PROCESSING)

        uniqueUrlList = extractUrlList(DIR_META_URL,processingFile)

        LOGGER.info("[EXTRACT] list url size {}".format(len(uniqueUrlList)))
        print("[EXTRACT] list url size {}".format(len(uniqueUrlList)))
        
        touchFile(processingFile,EXT_FILE_DONE)
        count = 0
        for url in uniqueUrlList:
            LOGGER.info("[REQUEST] download URL {}".format(url))
            print("[REQUEST] download URL {}".format(url))
            count +=1;
            LOGGER.info("[LOOP] request downloading number = {} ........".format(str(count)))
            print("[COUNT] request downloading number = {} ......".format(str(count)))
            if count==1:
                continue
            requestDownload(url,SAVE_AS,file)
            
    print("[COUNT] Download completed, count = {}".format(str(count)))
    LOGGER.info("Download completed, count = {}".format(str(count)))
    LOGGER.info("===========================================================")
    LOGGER.info("===========================================================")
    LOGGER.info("=======  Goodbye UdeDowner App  ======")
    LOGGER.info("=======  Goodbye UdeDowner App  ======")
    LOGGER.info("=======  Goodbye UdeDowner App  ======")
    LOGGER.info("===========================================================")
    LOGGER.info("===========================================================")
    print("*********** [STOP] UdeDowner App **************")


if __name__ == "__main__":
    main()
