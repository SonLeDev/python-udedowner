import urllib.request
import datetime
import time
from Logger import getLoggers

LOGGER = getLoggers("RequestDownloadUrl");

URL_TEST        = 'https://udemy-assets-on-demand.udemy.com/2017-06-15_03-22-45-a8420a8883ef18629160e10cc977bb37/WebHD_720p.mp4?nva=20190324162754&token=0abadd8055c4dac88e08e'
SAVE_AS_DEFAULT = '/Volumes/DEV/Download'

def requestDownload(url,saveAs,fileName):
    
    LOGGER.info('[==>] requestDownload')

    idDownload = time.time()
    saveAs = saveAs+"/"+str(idDownload)+"."+fileName+".mp4"
    
    LOGGER.info('[ID] download : {}'.format(idDownload))
    LOGGER.info('[START] download url : {}'.format(url))
    LOGGER.info('Start at : {0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()))
          
    urllib.request.urlretrieve(url,saveAs)
    
    LOGGER.info('ID download : {} '.format(idDownload))
    LOGGER.info('Completed at : {0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()))
    LOGGER.info('========================================')

